import { ApolloServer } from 'apollo-server';

import config from '../config';
import schema from './graph/root';

const apiManager = {

  async startServer() {

    const server = new ApolloServer({
      schema,
      playground: true,
      introspection: true,
    });

    // Config Server
    const { host, port } = config.api;
    const { url } = await server.listen({ host, port });
    console.log(`Catalog App running in ${url}!`);
  },
};

export default apiManager;
