import apiManager from './api/start';
import dbManager from './persistence/start';

async function launch() {

  try {
    await dbManager.startConnection();
    await apiManager.startServer();

  } catch (error) {
    console.error('Cannot launch app', error);
    throw new Error('Abort app launch!');
  }
}

launch()
  .catch((error) => {
    console.error('Something went wrong, check logs!', error.message);
  });
